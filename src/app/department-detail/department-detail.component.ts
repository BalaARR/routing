import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router,ParamMap} from '@angular/router';
@Component({
  selector: 'app-department-detail',
  template: `
      <h2> 

      You have selected department with id = {{departmentId}}
      </h2>
      <p>
      <button (click)="showOverview()">Overview</button>
      <button (click)="showContact()">Contact</button>
      <router-outlet></router-outlet>
      <p>
      <button (click)="goPrevious()">Previous</button>
      <button (click)="goNext()"> Next</button>
      </p>
      <div>
      <button (click)="goToDepartment()">BACK</button>
      </div>
  `,
  styles: []
})
export class DepartmentDetailComponent implements OnInit {
  public departmentId;
  public departmentName;
  constructor(private route:ActivatedRoute,private router: Router) { }

  ngOnInit() {
    /*let name=this.route.snapshot.paramMap.get('name');
    this.departmentName=name;*/

    //let id=parseInt(this.route.snapshot.paramMap.get('id'));
    //this.departmentId=id;

    this.route.paramMap.subscribe((params: ParamMap)=> {
      let id =parseInt(params.get('id'));
      this.departmentId=id;
    })
  }

  goPrevious(){
    let previousId=this.departmentId-1;
    this.router.navigate(['/departments',previousId]);
  }

  goNext(){
    let nextId=this.departmentId+1;
    this.router.navigate(['/departments',nextId]);
  }

  goToDepartment(){
    let selectedId=this.departmentId?this.departmentId:null;
    this.router.navigate(['/departments',{id: selectedId}])
  }

  showOverview(){
    this.router.navigate(['overview'],{relativeTo:this.route});
  }

  showContact(){
    this.router.navigate(['contact'],{relativeTo:this.route});
  }

}
